import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import AssetServiceSdk from 'asset-service-sdk';

angular
    .module('productRegistrationReviewCntrl',['ui.bootstrap']);
export default class productRegistrationReviewCntrl{
    constructor($scope,$q, $uibModal,$location,sessionManager, identityServiceSdk,assetServiceSdk,partnerSaleRegistrationServiceSdk) {

        var getReview=JSON.parse(window.localStorage.getItem("reviewList"));

        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
        ).then(
            accessToken => {
                $scope.loader=true;
                $q(
                    resolve =>
                        partnerSaleRegistrationServiceSdk
                            .getPartnerSaleRegistrationDraftWithDraftId(getReview, accessToken)
                            .then(
                                response =>
                                    resolve(response)
                            )
                ).then(PartnerCommercialSaleRegSynopsisView => {
                    $scope.loader=false;
                    console.log(' RESPONSE PartnerCommercialSaleRegSynopsisView::',PartnerCommercialSaleRegSynopsisView);
                    $scope.productsReviewList=PartnerCommercialSaleRegSynopsisView;
                });
            });
        var draftId=window.localStorage.getItem("draftId");
        $scope.productRegistrationReview_Next=function(){
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            partnerSaleRegistrationServiceSdk
                                .submitPartnerCommercialSaleRegDraft(draftId, accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(PartnerCommercialSaleRegSynopsisView => {
                       // $scope.loader=false;
                        console.log(' submit RESPONSE PartnerCommercialSaleRegSynopsisView::',PartnerCommercialSaleRegSynopsisView);
                        window.localStorage.setItem("SubmittedDraft",JSON.stringify(PartnerCommercialSaleRegSynopsisView));

                        var synopsisView = PartnerCommercialSaleRegSynopsisView;
                        var accountId = synopsisView.facilityId;
                        console.log("accountId....",accountId);
                        var assetIds = JSON.parse(window.localStorage.getItem("assetIds"));

                        var request={};
                        request.accountId = accountId;
                        request.assetIds = assetIds;
                        $q(
                            resolve =>
                                assetServiceSdk
                                    .updateAccountIdOfAssets(request, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(response => {
                            $scope.loader=false;
                            console.log("Updated facility id successfully");
                            $scope.modalInstance = $uibModal.open({
                                scope:$scope,
                                template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                '<div class="modal-body">Registration submitted successfully </div>' +
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="review_confirm()">Ok</button></div>',
                                size:'sm',
                                backdrop : 'static'
                            });
                        });
                    });
                });

        };
        $scope.review_confirm=function(){
            $location.path("/productRegistrationSubmit");
        };
        $scope.productRegistrationReview_Previous=function(){
            $location.path("/productRegistrationPricing");
        }
        $scope.Back_Drafts=function(){
            $location.path("/");
        };
    }
};

productRegistrationReviewCntrl.$inject=[
    '$scope',
    '$q',
    '$uibModal',
    '$location',
    'sessionManager',
    'identityServiceSdk',
    'assetServiceSdk',
    'partnerSaleRegistrationServiceSdk'
];