import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';
import {AddCommercialAccountReq} from 'account-service-sdk';
import {AddAccountContactReq} from 'account-contact-service-sdk';
import PartnerSaleRegistrationServiceSdk from 'partner-sale-registration-draft-service-sdk';
import ProductGroupServiceSdk from 'product-group-service-sdk';
import ProductLineServiceSdk from 'product-line-service-sdk';
import AssetServiceSdk from 'asset-service-sdk';
import {PartnerSaleInvoiceServiceSdk} from 'partner-sale-invoice-service-sdk';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';

angular
    .module('productRegistrationEditDraftCntrl',['ui.bootstrap']).filter('unique', function() {
    return function(input, key) {
        var unique = {};
        var uniqueList = [];
        for(var i = 0; i < input.length; i++){
            if(typeof unique[input[i][key]] == "undefined"){
                unique[input[i][key]] = "";
                uniqueList.push(input[i]);
            }
        }
        return uniqueList;
    };
});
export default class productRegistrationEditDraftCntrl{
    constructor($scope,$rootScope, $q, Upload,config, $uibModal,$location, sessionManager, identityServiceSdk, accountServiceSdk, accountContactServiceSdk,
                buyingGroupServiceSdk, managementCompanyServiceSdk, customerSourceServiceSdk, partnerRepServiceSdk,
                partnerRepAssociationServiceSdk,partnerSaleRegistrationServiceSdk,productLineServiceSdk,productGroupServiceSdk,partnerSaleInvoiceServiceSdk,assetServiceSdk,addPartnerRepModal) {

        $scope.isEdit=true;
        $rootScope.finalResponse = function() {
            $scope.responseLoad();
            $scope.editDetails();
            $scope.dealerRepDropdown();
            $scope.productGroupsList();
            //$scope.populateExistingProducts();
        };
        $scope.responseLoad=function(){
            $scope.addNewContact={};
            $scope.productRegistration={};
            $scope.addNewFacility={};
            $scope.loader=false;
            //populate facilities drop down
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            identityServiceSdk
                                .getUserInfo(accessToken)
                                .then(
                                    userInfo =>
                                        resolve(userInfo)
                                )
                    ).then(userInfo => {
                        $scope.accountId=userInfo._account_id;
                        $scope.loader=true;
                        $q(
                            resolve =>
                                accountServiceSdk
                                    .searchCommercialAccountsAssociatedWithPartnerAccountId(userInfo._account_id, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(facilities => {
                            console.log("facilities:", JSON.parse(JSON.stringify(facilities)));
                            $scope.facilities = JSON.parse(JSON.stringify(facilities));

                            angular.forEach($scope.facilities,function(val,key){
                                if(val.id==$scope.EditDraft.facilityId){
                                    $scope.productRegistration.selectedFacility=val;
                                }
                            });
                            $scope.onFacilitySelected();
                            $scope.loader=false;
                        });
                    });
                });


            //populate buying group dropdown
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            buyingGroupServiceSdk
                                .listBuyingGroups(accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(buyingGroups => {
                        $scope.loader=false;
                        console.log("buyingGroups:", buyingGroups.response);
                        $scope.buyingGroups = JSON.parse(buyingGroups.response);
                    });
                });

            //populate management companies dropdown

            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(resolve =>
                        managementCompanyServiceSdk
                            .listManagementCompanies(accessToken)
                            .then(
                                response =>
                                    resolve(response)
                            )
                    ).then(managementCompanies => {
                        $scope.loader=false;
                        console.log("managementCompanies:", managementCompanies.response);
                        $scope.managementCompanies = JSON.parse(managementCompanies.response);
                    });
                });

            //populate customer source dropdown

            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            customerSourceServiceSdk
                                .listCustomerSources(accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(customerSources => {
                        $scope.loader=false;
                        console.log("customerSources:", customerSources);
                        $scope.customerSources = customerSources;
                    });
                });


            //populate dealer rep dropdown
            $scope.dealerRepDropdown=function(){
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                identityServiceSdk
                                    .getUserInfo(accessToken)
                                    .then(
                                        userInfo =>
                                            resolve(userInfo)
                                    )
                        ).then(userInfo => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    partnerRepAssociationServiceSdk
                                        .listPartnerRepAssociationsWithPartnerId(userInfo._account_id, accessToken)
                                        .then(partnerRepAssociations =>
                                            resolve(partnerRepAssociations)
                                        )
                            ).then(partnerRepAssociations => {
                                $scope.loader=false;
                                console.log("partnerRepAssociations:", partnerRepAssociations);
                                var partnerRepIds = Array.from(partnerRepAssociations, (partnerRepAssociation) => {
                                        return partnerRepAssociation.repId;
                                    }
                                );

                                console.log("partnerRepIds:", partnerRepIds);

                                $q(
                                    resolve =>
                                        partnerRepServiceSdk
                                            .getPartnerRepsWithIds(partnerRepIds, accessToken)
                                            .then(partnerReps =>
                                                resolve(partnerReps)
                                            )
                                ).then(partnerReps => {
                                    $scope.loader=false;
                                    console.log("partnerReps:", partnerReps);
                                    $scope.dealers = partnerReps;
                                });
                            });
                        });
                    });
            };

            if(document.getElementById('selectedFacility').value == ""){
                $scope.contactIdButtonIsDisabled = true;
            }else{
                $scope.contactIdButtonIsDisabled = false;
            }


            $scope.onFacilitySelected = function() {
                $scope.loader=false;
                // console.log("id is",id);
                if($scope.productRegistration.selectedFacility==null)
                {
                    $scope.isDisabled=false;
                    $scope.facilityInfo={};
                    $scope.contactIdButtonIsDisabled = true;
                }
                else{
                    $q(
                        resolve =>
                            sessionManager
                                .getAccessToken()
                                .then(
                                    accessToken =>
                                        resolve(accessToken)
                                )
                    ).then(
                        accessToken => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    identityServiceSdk
                                        .getUserInfo(accessToken)
                                        .then(
                                            userInfo =>
                                                resolve(userInfo)
                                        )
                            ).then(userInfo => {
                                $scope.loader=true;
                                $q(
                                    resolve =>
                                        accountServiceSdk
                                            .getCommercialAccountWithId($scope.productRegistration.selectedFacility.id, accessToken)
                                            .then(
                                                response =>
                                                    resolve(response)
                                            )
                                ).then(facilityInfo => {
                                    console.log("facilityInfo:", JSON.parse(JSON.stringify(facilityInfo)));
                                    $scope.facilityInfo = JSON.parse(JSON.stringify(facilityInfo));
                                    $scope.loader=false;
                                });
                                $q(
                                    resolve =>
                                        accountContactServiceSdk
                                            .listAccountContactsWithAccountId($scope.productRegistration.selectedFacility.id, accessToken)
                                            .then(
                                                response =>
                                                    resolve(response)
                                            )
                                ).then(facilityContacts => {
                                    console.log("facilityContacts:", JSON.parse(JSON.stringify(facilityContacts)));
                                    $scope.facilityContacts = JSON.parse(JSON.stringify(facilityContacts));
                                    $scope.loader=false;

                                });
                            });
                        });
                    $scope.isDisabled=true;
                    $scope.contactIdButtonIsDisabled = false;
                }
            };

            //AddNewContactInfo Begin
            //AddNewContcatInfo End
            $scope.addNewContact_submit = function(isValid) {
                $scope.submitted=true;
                $scope.loader=false;
                if(isValid){
                    $scope.loader=true;
                    var addAccountContactReq = new AddAccountContactReq($scope.productRegistration.selectedFacility.id,
                        $scope.addNewContact.firstName,
                        $scope.addNewContact.lastName,
                        $scope.addNewContact.contactPhone,
                        $scope.addNewContact.contactEmail,
                        $scope.productRegistration.selectedFacility.address.countryIso31661Alpha2Code,
                        $scope.productRegistration.selectedFacility.address.regionIso31662Code
                    );
                    $q(
                        resolve =>
                            sessionManager
                                .getAccessToken()
                                .then(
                                    accessToken =>
                                        resolve(accessToken)
                                )
                    ).then(
                        accessToken => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    accountContactServiceSdk
                                        .addAccountContact(addAccountContactReq, accessToken)
                                        .then(
                                            response =>
                                                resolve(response)
                                        )
                            ).then(response => {
                                $scope.loader=false;
                                console.log("add account contact response :", response);
                                $scope.modalInstance.close();
                                $scope.modalInstance = $uibModal.open({
                                    scope:$scope,
                                    template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                    '<div class="modal-body">ContactInfo Added Successfully</div>' +
                                    '<div class="modal-footer">' +
                                    '<button class="btn btn-primary" type="button" ng-click="ok_addContact()">ok</button></div>',
                                    size:'sm',
                                    backdrop : 'static'
                                });
                                $scope.ok_addContact=function(){
                                    $scope.modalInstance.close();
                                    $scope.submitted=false;
                                    $scope.addNewContact={};
                                    $scope.onFacilitySelected();
                                    //$scope.getFacilityContactInfo(accessToken);
                                }
                            });
                        }
                    );

                }
            };
            $scope.newContact_cancel=function(){
                $scope.submitted=false;
                $scope.addNewContact={};
                $scope.modalInstance.dismiss('cancel');
            };

            //Start date picker
            $scope.today = function() {
                $scope.sellDate = new Date();
                $scope.installDate = new Date();
            };
            $scope.today();

            $scope.clear = function () {
                $scope.sellDate = null;
                $scope.installDate = null;
            };

            // Disable weekend selection


            $scope.open = function($event) {
                $scope.status.opened = true;
            };
            $scope.open1 = function($event) {
                $scope.status.opened1 = true;
            };

            $scope.setDate = function(year, month, day) {
                $scope.sellDate = new Date(year, month, day);
                $scope.installDate = new Date(year, month, day);
            };

            $scope.dateOptions = {
                formatYear: 'yy',
                startingDay: 1
            };

            $scope.formats = ['dd-MMMM-yyyy', 'MM/dd/yyyy', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[1];

            $scope.status = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 2);
            $scope.events =
                [
                    {
                        date: tomorrow,
                        status: 'full'
                    },
                    {
                        date: afterTomorrow,
                        status: 'partially'
                    }
                ];

            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0,0,0,0);

                    for (var i=0;i<$scope.events.length;i++){
                        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            };

            //End datepicker
            //response for facility dropdown
            ///
            $scope.addFacility = function (size) {
                $scope.submitted=false;
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/addFacility.html',
                    size: size,
                    backdrop : 'static'

                });
            };
            $scope.newContact= function (size) {
                $scope.submitted=false;
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/addNewContact.html',
                    size: size,
                    backdrop : 'static'

                });
            };
            $scope.productGroupsList=function(){
                //populate productLinesList companies dropdown
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                productGroupServiceSdk
                                    .listProductGroups(accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    ).catch(function(e){
                                    console.log("error in ProductGroups......",e);
                                    $("#err").show().fadeOut(3000);
                                    $scope.loader=false;
                                    $scope.errorMessage=e.response;
                                })
                        ).then(listProductGroups => {
                            $scope.loader=false;
                            console.log("ProductGroups :", listProductGroups);
                            $scope.productsTypeList = listProductGroups;
                        });
                    });
            }
            $scope.prodListJson ={};
            $scope.idSelectedVote = {};
            var selectedRecord = [];
            $scope.addProduct=function(size){
                $scope.AddProdctGridList=[];
                selectedRecord = [];
                $scope.isEmptyArray=false;
                $scope.isArrayExist=false;
                $scope.noComponentsAdded = false;
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/AddProduct.html',
                    size: size,
                    backdrop : 'static'

                });
                $scope.loader=false;
                $scope.prodJson=[];


            };
            //search using serial number
            $scope.loadAssetsList=function(){
                var partialSerialNumber=$('#serialNumberId').val();
                console.log("data",partialSerialNumber);
                $scope.isEmptyArray=false;
                var alreadyExists = false;
                $('#productType_select').hide();
                // console.log("example",JSON.parse(JSON.stringify($scope.productsType.product)));
                if(partialSerialNumber!=''){
                    angular.forEach($scope.AddProdctGridList,function(val,key){
                        if(!alreadyExists && val.serialNumber.indexOf(partialSerialNumber.toUpperCase()) > -1){
                            alreadyExists = true;
                        }
                    });
                    if(!alreadyExists) {
                        //populate productLinesList companies dropdown
                        $q(
                            resolve =>
                                sessionManager
                                    .getAccessToken()
                                    .then(
                                        accessToken =>
                                            resolve(accessToken)
                                    )
                        ).then(
                            accessToken => {
                                $scope.loader = true;
                                $q(
                                    resolve =>
                                        identityServiceSdk
                                            .getUserInfo(accessToken)
                                            .then(
                                                userInfo =>
                                                    resolve(userInfo)
                                            )
                                ).then(userInfo => {
                                    $scope.loader = true;
                                    $q(
                                        resolve =>
                                            assetServiceSdk
                                                .listAssetsWithSerialNumber($scope.accountId, partialSerialNumber, accessToken)
                                                .then(
                                                    response =>
                                                        resolve(response)
                                                ).catch(function (e) {
                                                console.log("error in assets......", e);
                                                $("#err").show().fadeOut(8000);
                                                $scope.loader = false;
                                                $scope.errorMessage = e.response;
                                            })
                                    ).then(AssetSynopsisView => {
                                        $scope.loader = false;
                                        if (AssetSynopsisView.length <= 0) {
                                            $scope.isEmptyArray = true;
                                            $scope.AddProdctGridList = [];
                                        }
                                        else {
                                            $scope.isEmptyArray = false;
                                            $scope.isArrayExist = true;
                                            $scope.AddProdctGridList = JSON.parse(JSON.stringify(AssetSynopsisView));
                                            console.log("$scope.AddProdctGridList", $scope.AddProdctGridList);
                                        }

                                    });
                                })
                            });
                    }
                }
            };

            /*$scope.removeSaleLineItem = function(lineItemIndex,lineItemId){
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    template: '<div class="modal-header"> <h4 class="modal-title">Warning !</h4></div>' +
                    '<div class="modal-body">Do you want to delete the product ?</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary" type="button" ng-click="removeSaleLineItem_confirm()">Yes</button>' +
                    '<button class="btn btn-warning" type="button" ng-click="cancel_close()">No</button></div>',
                    size:'sm'
                });

                $scope.removeSaleLineItem_confirm = function(){
                    Object.keys($scope.prodListJson).forEach(function (key) {
                        //if(key[0]==letter) delete obj[key];////without regex
                        if(key.match('^'+lineItemIndex)) delete $scope.prodListJson[key];//with regex

                    });
                    console.log("prodListJson ",$scope.prodListJson);
                    console.log("lineItemIndex ",lineItemIndex);
                    if(lineItemId!=undefined) {
                        $scope.removeSaleLineItemWithId(lineItemId);
                    }
                    $scope.modalInstance.close();
                }
            }*/
            $scope.removeSimpleLineItem = function(simpleitem){
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    template: '<div class="modal-header"> <h4 class="modal-title">Warning !</h4></div>' +
                    '<div class="modal-body">Do you want to delete the product ?</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary" type="button" ng-click="removeSimpleLineItem_confirm()">Yes</button>' +
                    '<button class="btn btn-warning" type="button" ng-click="cancel_close()">No</button></div>',
                    size:'sm'
                });

                $scope.removeSimpleLineItem_confirm = function(){
                    console.log("simpleitem : ",simpleitem);
                    angular.forEach($scope.prodListJson,function(prodObj,prodKey){

                        if(!(prodObj.length>1) && (prodObj[0] == simpleitem)){
                            console.log("matched ");
                            delete $scope.prodListJson[prodKey];
                        }
                    });
                    console.log("$scope.prodListJson : ",$scope.prodListJson);
                    if(simpleitem.id==undefined){
                        if ($scope.EditDraft.simpleLineItems.indexOf(simpleitem) != -1) {
                            $scope.EditDraft.simpleLineItems.splice($scope.EditDraft.simpleLineItems.indexOf(simpleitem), 1);
                        }
                        $scope.modalInstance.close();
                    }else{
                        $scope.removeSaleLineItem(simpleitem.id);
                        if ($scope.EditDraft.simpleLineItems.indexOf(simpleitem) != -1) {
                            $scope.EditDraft.simpleLineItems.splice($scope.EditDraft.simpleLineItems.indexOf(simpleitem), 1);

                        }
                        $scope.modalInstance.close();
                    }

                }
            }
            $scope.removeCompositeLineItem = function(compositeitem){
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    template: '<div class="modal-header"> <h4 class="modal-title">Warning !</h4></div>' +
                    '<div class="modal-body">Do you want to delete the product ?</div>' +
                    '<div class="modal-footer">' +
                    '<button class="btn btn-primary" type="button" ng-click="removeCompositeLineItem_confirm()">Yes</button>' +
                    '<button class="btn btn-warning" type="button" ng-click="cancel_close()">No</button></div>',
                    size:'sm'
                });

                $scope.removeCompositeLineItem_confirm = function(){
                    console.log("compositeitem : ",compositeitem);
                    angular.forEach($scope.prodListJson,function(prodObj,prodKey){

                        if((prodObj.length>1) && (prodObj == compositeitem.components)){
                            console.log("matched ");
                            delete $scope.prodListJson[prodKey];
                        }
                    });
                    console.log("$scope.prodListJson : ",$scope.prodListJson);
                    if(compositeitem.id==undefined){
                        if ($scope.EditDraft.compositeLineItems.indexOf(compositeitem) != -1) {
                            $scope.EditDraft.compositeLineItems.splice($scope.EditDraft.compositeLineItems.indexOf(compositeitem), 1);
                        }
                        $scope.modalInstance.close();
                    }else{
                        $scope.removeSaleLineItem(compositeitem.id);
                        if ($scope.EditDraft.compositeLineItems.indexOf(compositeitem) != -1) {
                            $scope.EditDraft.compositeLineItems.splice($scope.EditDraft.compositeLineItems.indexOf(compositeitem), 1);
                        }
                        $scope.modalInstance.close();
                    }

                }
            }

            $scope.cancel_close=function(){
                $scope.modalInstance.dismiss('cancel');
            };
            $scope.removeSaleLineItem = function(id){
                console.log("itemId : ",id);
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                partnerSaleRegistrationServiceSdk
                                    .removeSaleLineItemFromPartnerSaleRegDraft(id, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(reponse => {
                            $scope.loader=false;
                            console.log(' delete line item reponse ::',reponse);
                        });
                    });

            }
            $scope.Back_Drafts=function(){
                $location.path("/");
            };

            $scope.setColor=function(len){
                if(len>1){
                    return "bgColor";
                }
            }

            /**{
            "7":[{"id","gid,"},{"id","gid,"}]
            "8":[{"id"}]
        }**/

            $scope.prodJson=[];

            var i=0;

            $scope.Selected_ProductLine=function(record){
                selectedRecord = record;
                $scope.idSelectedVote = record.assetId;
            }
            $scope.addComponent = function () {
                if(Object.keys(selectedRecord).length>0 && $scope.prodJson.indexOf(selectedRecord)==-1){
                    $scope.prodJson.push(selectedRecord);
                    $scope.noComponentsAdded = false;
                }
            };

            $scope.saveandClose = function () {
                $scope.noComponentsAdded = false;
                if(Object.keys($scope.prodJson).length>0){
                    var existingProducts =[];
                    var tempProducts =[];
                    if(Object.keys($scope.prodListJson).length>0){
                        i=Object.keys($scope.prodListJson).length;
                        angular.forEach(Object.values($scope.prodListJson),function(existingProduct){
                            existingProducts = existingProducts.concat(existingProduct);
                        });

                        angular.forEach($scope.prodJson,function(addedProduct){
                            if(!isExits(existingProducts,addedProduct)){
                                tempProducts.push(addedProduct);
                            }
                        });
                    }else{
                        $scope.prodListJson[i] = $scope.prodJson;
                        i++;
                    }
                    if(tempProducts.length>0){
                        $scope.prodListJson[i] = tempProducts;
                        i++;
                    }
                    console.log("prodListJson ",$scope.prodListJson);
               /* $scope.compositeLineItems=[];
                $scope.simpleLineItems=[];
                var totalCompositeLineItems = [];
                angular.forEach($scope.prodListJson,function(lineItem,k){
                    if(lineItem.length>1){
                        var json={};
                        json["components"] = lineItem;
                        angular.forEach($scope.EditDraft.compositeLineItems,function(compositeLineItem,key){
                            totalCompositeLineItems =  totalCompositeLineItems.concat(compositeLineItem.components);
                        });
                        if (!isCompositeExits(totalCompositeLineItems, lineItem)) {
                            $scope.compositeLineItems.push(json);
                            $scope.EditDraft.compositeLineItems=$scope.EditDraft.compositeLineItems.concat($scope.compositeLineItems);
                        }

                    }
                    else{
                            if(!isExits($scope.EditDraft.simpleLineItems,lineItem[0])){
                                $scope.simpleLineItems.push(lineItem[0]);
                                $scope.EditDraft.simpleLineItems=$scope.EditDraft.simpleLineItems.concat($scope.simpleLineItems);
                            }



                    }
                });
*/
                    $scope.compositeLineItems=[];
                    $scope.simpleLineItems=[];
                    var totalCompositeLineItems = [];
                    angular.forEach($scope.prodListJson,function(lineItem,k){
                        if(lineItem.length>1){
                            var json={};
                            json["components"] = lineItem;
                            angular.forEach($scope.EditDraft.compositeLineItems,function(value,key){
                                totalCompositeLineItems =  totalCompositeLineItems.concat(value.components);
                            });
                            if (!isCompositeExits(totalCompositeLineItems, lineItem)) {
                                $scope.compositeLineItems.push(json);
                                $scope.EditDraft.compositeLineItems=$scope.EditDraft.compositeLineItems.concat($scope.compositeLineItems);
                            }

                        }
                        else{
                            if(!isExits($scope.EditDraft.simpleLineItems,lineItem[0])){
                                $scope.simpleLineItems.push(lineItem[0]);
                            }

                            $scope.EditDraft.simpleLineItems=$scope.EditDraft.simpleLineItems.concat($scope.simpleLineItems);
                        }
                    });
                console.log("prodListJson",$scope.prodListJson);
                $scope.modalInstance.close();
                $("#error_product").hide();
                }else{
                    $scope.noComponentsAdded = true;
                }
                selectedRecord=[];
                $scope.prodJson=[];
                $scope.idSelectedVote={};
            };

            function isCompositeExits(components,selectedComponents){
                var isValueExits = false;
                angular.forEach(components,function(value){
                    angular.forEach(selectedComponents,function(selValue){
                    if(value.serialNumber == selValue.serialNumber){
                        isValueExits =  true;
                    }
                });
                });
                return isValueExits;
            }

            $scope.saveandAnother = function (size) {
                $scope.modalInstance.close();
                $scope.saveandClose();
                $scope.prodJson=[];
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/AddProduct.html',
                    size: size,
                    backdrop : 'static'

                });
            };
            function isExits(arr,val){
                var isValueExits = false;
                angular.forEach(arr,function(value){
                    if(val.serialNumber == value.serialNumber){
                        isValueExits =  true;
                    }
                });
                return isValueExits;
            }
            $scope.AddProductnotListed = function (size) {

                $scope.modalInstance.close();
                $scope.modalInstance = $uibModal.open({
                    scope:$scope,
                    templateUrl: 'templates/popupModals/addProductnotListed.html',
                    size: size,
                    backdrop : 'static'

                });
                // $scope.productLines=$scope.productsTypeList;
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                identityServiceSdk
                                    .getUserInfo(accessToken)
                                    .then(
                                        userInfo =>
                                            resolve(userInfo)
                                    )
                        ).then(userInfo => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    productLineServiceSdk
                                        .listProductLines(accessToken)
                                        .then(
                                            response =>
                                                resolve(response)
                                        )
                            ).then(productLineIds => {
                                $scope.loader=false;
                                console.log("productLines:", productLineIds);
                                $scope.productLines = productLineIds;
                                $scope.lineIDs=[];
                                angular.forEach($scope.productLineIDs,function(v,k){
                                    $scope.lineIDs.push(v.id);
                                });

                            });

                        });
                    });


                //$scope.modalInstance.close();
            };
            $scope.addNewProductnotListed={};
            $scope.addNewProductLine=function(){
                var req={};
                req.accountId=$scope.accountId;
                req.productLineId= $scope.addNewProductnotListed.productLine.id;
                req.serialNumber=$scope.addNewProductnotListed.productSerialNumber;
                req.description=$scope.addNewProductnotListed.productDescription;

                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $scope.loader=true;
                        $q(
                            resolve =>
                                identityServiceSdk
                                    .getUserInfo(accessToken)
                                    .then(
                                        userInfo =>
                                            resolve(userInfo)
                                    )
                        ).then(userInfo => {
                            $scope.loader=true;
                            $q(
                                resolve =>
                                    assetServiceSdk
                                        .addAsset(req, accessToken)
                                        .then(
                                            response =>
                                                resolve(response)
                                        )
                            ).then(AssestResponse => {
                                $scope.loader=false;
                                $scope.modalInstance.close();
                                console.log("productAddAseetresponse:", AssestResponse);
                                //$scope.AddProdctGridList = AssetSynopsisView;
                            });

                        });
                    });

            };


//next submit
            $scope.handleFileSelection = function($files) {
                $scope.invoiceFileToUpload = $files[0];
            }

            $scope.draftRequest=function(){
                $scope.compositeLineItems=[];
                $scope.simpleLineItems=[];
                $scope.assetIds=[];
            /*    angular.forEach($scope.EditDraft.compositeLineItems,function(compositeItem){
                    angular.forEach(compositeItem.components,function(component){
                        $scope.assetIds.push(component.assetId);
                    });
                });

                angular.forEach($scope.EditDraft.simpleLineItems,function(simpleItem){
                    $scope.assetIds.push(simpleItem.assetId);
                });*/

                angular.forEach($scope.prodListJson,function(lineItem,k){
                    if(lineItem.length>1){
                        var json={};
                        json["components"] = lineItem;

                            $scope.compositeLineItems.push(json);
                            //$scope.assetIds.push(lineItem.assetId);
                        angular.forEach(lineItem,function(component){
                            $scope.assetIds.push(component.assetId);
                        });
                            //$scope.EditDraft.compositeLineItems=$scope.EditDraft.compositeLineItems.concat($scope.compositeLineItems);
                    }
                    else{

                            $scope.simpleLineItems.push(lineItem[0]);
                        $scope.assetIds.push(lineItem[0].assetId);
                            //$scope.EditDraft.simpleLineItems=$scope.EditDraft.simpleLineItems.concat($scope.simpleLineItems);
                      }
                });
                console.log('assetIds',$scope.assetIds);
                window.localStorage.setItem("assetIds",JSON.stringify($scope.assetIds));
                var request={};
                var buyingGroupId = ($scope.productRegistration.buyingGroup==undefined) ? null: $scope.productRegistration.buyingGroup;
                request.id=$scope.EditDraft.id;
                request.partnerAccountId=$scope.accountId;
                request.facilityName=$scope.productRegistration.selectedFacility.name;
                request.customerBrandName=$scope.facilityInfo.customerBrand;
                request.facilityId=$scope.productRegistration.selectedFacility.id;
                request.buyingGroupId=buyingGroupId;
                ($scope.productRegistration.managementCompany==undefined)? request.managementCompanyId=null:request.managementCompanyId=$scope.productRegistration.managementCompany;
                ($scope.productRegistration.customerSource==undefined)? request.customerSourceId=null:request.customerSourceId=$scope.productRegistration.customerSource;
                request.installDate=$scope.productRegistration.installDate;
                request.sellDate=$scope.productRegistration.sellDate;
                request.invoiceNumber=$scope.productRegistration.invoiceNumber;
                request.facilityContactId=$scope.productRegistration.contactInfo;
                ($scope.productRegistration.dealerRep==undefined||$scope.productRegistration.dealerRep=="")? request.partnerRepUserId=null:request.partnerRepUserId=$scope.productRegistration.dealerRep;
                request.simpleLineItems=$scope.EditDraft.simpleLineItems;
                request.compositeLineItems=$scope.EditDraft.compositeLineItems;
                request.invoiceUrl=$scope.productRegistration.invoiceUrl;
                //request.invoiceUrl=null;
                request.isSubmitted=false;
                console.log("total::",request);
                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        $q(
                            resolve =>
                                partnerSaleRegistrationServiceSdk
                                    .updatePartnerCommercialSaleRegDraft(request,$scope.EditDraft.id, accessToken)
                                    .then(
                                        response =>
                                            resolve(response)
                                    )
                        ).then(PartnerCommercialSaleRegSynopsisView => {
                            console.log(' RESPONSE in UPDATE ::',PartnerCommercialSaleRegSynopsisView);
                            $scope.draftId = PartnerCommercialSaleRegSynopsisView.id;
                            window.localStorage.setItem("draftId",JSON.stringify(PartnerCommercialSaleRegSynopsisView.id));
                            $scope.uploadInvoice();

                        });
                    });

                //$scope.productRegistrationForm.$setPristine();
                // $scope.productRegistration={};
                $scope.isDisabled=false;
                $scope.compositeLineItems=[];
                $scope.simpleLineItems=[];
            };
            $scope.uploadInvoice = function(){

                $q(
                    resolve =>
                        sessionManager
                            .getAccessToken()
                            .then(
                                accessToken =>
                                    resolve(accessToken)
                            )
                ).then(
                    accessToken => {
                        var partnerSaleInvoiceNumber = $scope.productRegistration.invoiceNumber;
                        var partnerSaleRegId = $scope.draftId;
                        var baseUrl = config.partnerSaleInvoiceServiceSdkConfig.precorConnectApiBaseUrl;
                        console.log("invoicenumber",partnerSaleInvoiceNumber);
                        console.log("partnerSaleRegId",partnerSaleRegId);

                        if($scope.invoiceFileToUpload) {
                            /**
                             * TODO: Try to move this to javascript SDK.
                             */
                            Upload.upload({
                                url: baseUrl + '/partner-sale-invoices',
                                headers: {
                                    'Authorization': `Bearer ${accessToken}`
                                },
                                data: {number:partnerSaleInvoiceNumber,partnerSaleRegistrationId:partnerSaleRegId,file: $scope.invoiceFileToUpload}
                            }).then(response => {
                                    console.log("upload succeeded:", response.data);
                                    $scope.invoiceId=response.data;
                                    $q(
                                        resolve =>
                                            partnerSaleInvoiceServiceSdk
                                                .getPartnerSaleInvoiceWithId($scope.invoiceId, accessToken)
                                                .then(
                                                    PartnerSaleInvoiceView =>
                                                        resolve(PartnerSaleInvoiceView)
                                                )
                                    ).then(PartnerSaleInvoiceView => {
                                        console.log("invoiceURL Response:", PartnerSaleInvoiceView._fileUrl);
                                        //$scope.productRegistration.invoiceUrl=PartnerSaleInvoiceView._fileUrl;
                                        //$scope.draftRequest();
                                        var request = {};

                                        request.id = partnerSaleRegId;
                                        request.invoiceUrl = PartnerSaleInvoiceView._fileUrl;
                                        $q(
                                            resolve =>
                                                partnerSaleRegistrationServiceSdk
                                                    .updateSaleInvoiceUrlOfPartnerSaleRegDraft(request, accessToken)
                                                    .then(
                                                        response =>
                                                            resolve(response)
                                                    )
                                        ).then(response => {
                                            console.log("update invoiceURL Response:", response);
                                            //$scope.productRegistration.invoiceUrl=PartnerSaleInvoiceView._fileUrl;
                                            //$scope.draftRequest();
                                            $scope.loader=false;
                                            $location.path('/productRegistrationPricing');
                                            $scope.productRegistrationForm.$setPristine();
                                        });
                                    });

                                },function (error) {
                                    console.log("Invoice upload error :", error);
                                    $("#err").show().fadeOut(3000);
                                    $scope.loader=false;
                                    $scope.errorMessage=e.response;
                                }
                            );

                        }
                        else{
                            console.log("file is undefined");
                            $scope.loader=false;
                            $location.path('/productRegistrationPricing');
                            $scope.productRegistrationForm.$setPristine();
                            //$scope.productRegistration.invoiceUrl=null;
                            //$scope.draftRequest();
                        }

                    }
                );
            }
            $scope.save_next_submit=function(){
                var error=false;

                if(!$scope.productRegistration.selectedFacility){ $scope.productRegistrationForm.selectFacility.$dirty = true; $scope.productRegistrationForm.selectFacility.$invalid = true ; error = true;}
                if(!$scope.productRegistration.contactInfo){ $scope.productRegistrationForm.contactInfo.$dirty = true; $scope.productRegistrationForm.contactInfo.$invalid = true ; error = true;}
                if(!$scope.productRegistration.sellDate){ $scope.productRegistrationForm.sellDate.$dirty = true; $scope.productRegistrationForm.sellDate.$invalid = true ; error = true;}
                if(!$scope.productRegistration.installDate){ $scope.productRegistrationForm.installDate.$dirty = true; $scope.productRegistrationForm.installDate.$invalid = true ; error = true;}
                if(!$scope.productRegistration.invoiceNumber){ $scope.productRegistrationForm.invoiceNumber.$dirty = true; $scope.productRegistrationForm.invoiceNumber.$invalid = true ; error = true;}
                if(!($scope.EditDraft.simpleLineItems.length+$scope.EditDraft.compositeLineItems.length)>0){
                    $("#error_product").show();
                    error=true;
                }
                /*else if(!$scope.prodListJson){
                    console.log("prodListJson fail");
                    $("#error_product").show();
                    error=true;
                }*/
                if (!error){
                    $scope.loader=true;

                    $scope.draftRequest();

                }


            };
           /* $scope.Next_success=function(){
                $location.path('/productRegistrationPricing');
            }*/
            $scope.saveNext_cancel=function(){
                $location.path('/');
            };


            $scope.remove_line_item=function(i){
                $scope.prodJson.splice(i,1);
            }
            $scope.Back_Drafts=function(){
                $location.path("/");
            }
            $scope.ok = function () {
                $scope.modalInstance.close();
            };

            $scope.cancel = function () {
                $scope.modalInstance.dismiss('cancel');
            };

            $scope.showAddPartnerRepModal=function() {
                $scope.loader = true;
                var moduleInstanceResult = addPartnerRepModal.show();
                console.log('moduleInstanceResult',moduleInstanceResult);
                moduleInstanceResult
                    .then(() =>{
                            // to load updated dealer reps
                        $scope.dealerRepDropdown();

                        },() => {
                            console.log('onRejected');
                            $scope.loader=false;
                        }
                    );
            }
        }

        $scope.editDetails=function(){
            console.log("EditedDraftsListzzz",JSON.parse(window.localStorage.getItem("EditedDrafts")));
            $scope.EditDraftID=JSON.parse(window.localStorage.getItem("EditedDrafts"));
            console.log("draftsResponse",$scope.EditDraftID);
            $q(
                resolve =>
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                resolve(accessToken)
                        )
            ).then(
                accessToken => {
                    $scope.loader=true;
                    $q(
                        resolve =>
                            partnerSaleRegistrationServiceSdk
                                .getPartnerSaleRegistrationDraftWithDraftId($scope.EditDraftID, accessToken)
                                .then(
                                    response =>
                                        resolve(response)
                                )
                    ).then(PartnerCommercialSaleRegSynopsisView => {
                        $scope.loader=false;
                        console.log(' RESPONSE PartnerCommercialSaleRegSynopsisView::',PartnerCommercialSaleRegSynopsisView);
                        $scope.EditDraft=PartnerCommercialSaleRegSynopsisView;
                        $scope.productRegistration.invoiceNumber=$scope.EditDraft.invoiceNumber;
                        $scope.productRegistration.invoiceUrl=$scope.EditDraft.invoiceUrl;
                        $scope.productRegistration.installDate=$scope.EditDraft.installDate;
                        $scope.productRegistration.sellDate=$scope.EditDraft.sellDate;
                        $scope.productRegistration.customerSource=$scope.EditDraft.customerSourceId;
                        $scope.productRegistration.buyingGroup=$scope.EditDraft.buyingGroupId;
                        $scope.productRegistration.contactInfo=$scope.EditDraft.facilityContactId;
                        $scope.productRegistration.managementCompany=$scope.EditDraft.managementCompanyId;
                        $scope.productRegistration.dealerRep=$scope.EditDraft.partnerRepUserId;
                        $scope.populateExistingProducts();

                    });
                });


            console.log("$scope.productRegi", $scope.productRegistration);
        };

        $scope.populateExistingProducts = function(){
            console.log("in populate s",$scope.EditDraft.simpleLineItems);
            console.log("in populate c ",$scope.EditDraft.compositeLineItems);
            $scope.prodListJson = {};
            $scope.lineItemIds = {};
            var count =0;
            var simpleItemArray = [];
            angular.forEach($scope.EditDraft.simpleLineItems,function(simplelineitem,index){
                simpleItemArray = [];
                simpleItemArray.push(simplelineitem);
                $scope.prodListJson[index] = simpleItemArray;
                $scope.lineItemIds[index]= simplelineitem.id;
            });
            count = Object.keys($scope.prodListJson).length;
            angular.forEach($scope.EditDraft.compositeLineItems,function(compositelineitem){
                console.log("iddssssssss are",compositelineitem.id);
                $scope.prodListJson[count] = compositelineitem.components;
                $scope.lineItemIds[count] = compositelineitem.id;
                count++;
            });

            console.log("$scope.prodListJson ",$scope.prodListJson);
            console.log("$scope.lineItemIds ",$scope.lineItemIds);

        };


    }
};
productRegistrationEditDraftCntrl.$inject=[
    '$scope',
    '$rootScope',
    '$q',
    'Upload',
    'config',
    '$uibModal',
    '$location',
    'sessionManager',
    'identityServiceSdk',
    'accountServiceSdk',
    'accountContactServiceSdk',
    'buyingGroupServiceSdk',
    'managementCompanyServiceSdk',
    'customerSourceServiceSdk',
    'partnerRepServiceSdk',
    'partnerRepAssociationServiceSdk',
    'partnerSaleRegistrationServiceSdk',
    'productLineServiceSdk',
    'productGroupServiceSdk',
    'partnerSaleInvoiceServiceSdk',
    'assetServiceSdk',
    'addPartnerRepModal'
];