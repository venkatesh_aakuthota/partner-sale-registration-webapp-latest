import angular from 'angular';
import SessionManager from 'session-manager';
import {AddCommercialAccountReq} from 'account-service-sdk';
import {PostalAddress} from 'postal-object-model';
import {AddAccountContactReq} from 'account-contact-service-sdk';
function facilityController($scope,$rootScope, $q, $uibModal, sessionManager, iso3166Sdk, customerSegmentServiceSdk, customerBrandServiceSdk, accountServiceSdk, accountContactServiceSdk,$timeout) {
    console.log("inside facilityController");

    $scope.addNewFacility={};
    $scope.loader=false;
    $q(
        resolve =>
            iso3166Sdk
                .listCountries()
                .then(
                    countries =>
                        resolve(countries)
                )
    ).then(
        countries => {
            console.log("countries:", countries);
            $scope.countries = countries;
        }
    );


    $q(
        resolve =>
            sessionManager
                .getAccessToken()
                .then(
                    accessToken =>
                        resolve(accessToken)
                )
    ).then(
        accessToken => {
            $scope.loader=true;
            $q(
                resolve =>
                    customerSegmentServiceSdk
                        .getLevel1CustomerSegment(accessToken)
                        .then(
                            customerTypes =>
                                resolve(customerTypes)
                        )
            ).then(customerTypes => {
                $scope.loader=false;
                    console.log("customer types:", customerTypes);
                    $scope.customerTypes = customerTypes;
            });
        }
    );


    $scope.onCountrySelected = function() {
        console.log("selected country alpha2Code:", $scope.addNewFacility.selectedCountry.alpha2Code);

        $q(resolve =>
            iso3166Sdk
                .listSubdivisionsWithAlpha2CountryCode($scope.addNewFacility.selectedCountry.alpha2Code)
                .then(
                    regions =>
                        resolve(regions)
                )
        ).then(
            regions => {
                console.log("regions:", regions);
                $scope.regions = regions;
            }
        );
    };

    $scope.onCustomerTypeSelected = function() {
        console.log("selected customer type ID:", $scope.addNewFacility.selectedCustomerType.id);
        $scope.customerSubTypes=[];
        $scope.addNewFacility.selectedCustomerSubType={};
        $scope.customerSubSubTypes=[];
        $scope.addNewFacility.selectedCustomerSubSubType={};
        $scope.facilityBrands=[];
        $scope.addNewFacility.selectedFacilityBrand={};
        $scope.loader=true;
        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
        ).then(
            accessToken => {
                $scope.loader=true;
                console.log("accessToken:", accessToken);
                $q(
                    resolve =>
                        customerSegmentServiceSdk
                            .getLevel2CustomerSegmentWithType($scope.addNewFacility.selectedCustomerType.id, accessToken)
                            .then(
                                customerSubTypes =>
                                    resolve(customerSubTypes)
                            )
                ).then(customerSubTypes => {
                    $scope.loader=false;
                    console.log("customerSubTypes :", customerSubTypes);
                    $scope.customerSubTypes = customerSubTypes;
                });
                $q(
                    resolve =>
                        customerBrandServiceSdk
                            .listLevel1CustomerBrandsWithSegmentId($scope.addNewFacility.selectedCustomerType.id, accessToken)
                            .then(
                                facilityBrands =>
                                    resolve(facilityBrands)
                            )
                ).then(facilityBrands => {
                    $scope.loader=false;
                    console.log("facilityBrands :", facilityBrands);
                    $scope.facilityBrands = facilityBrands;
                });
            }
        );

    }

    $scope.onCustomerSubTypeSelected = function() {
        console.log("selected customer sub type ID:", $scope.addNewFacility.selectedCustomerSubType.id);
        $scope.customerSubSubTypes=[];
        $scope.addNewFacility.selectedCustomerSubSubType={};
        $scope.loader=true;
        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
        ).then(
            accessToken => {

                console.log("accessToken:", accessToken);
                $q(
                    resolve =>
                        customerSegmentServiceSdk
                            .getLevel3CustomerSegmentWithSubType($scope.addNewFacility.selectedCustomerSubType.id, accessToken)
                            .then(
                                customerSubSubTypes =>
                                    resolve(customerSubSubTypes)
                            )
                ).then(customerSubSubTypes => {
                    $scope.loader=false;
                    console.log("customerSubSubTypes :", customerSubSubTypes);
                    $scope.customerSubSubTypes = customerSubSubTypes;
                });
                $q(
                    resolve =>
                        customerBrandServiceSdk
                            .listLevel1CustomerBrandsWithSegmentId($scope.addNewFacility.selectedCustomerSubType.id, accessToken)
                            .then(
                                facilityBrands =>
                                    resolve(facilityBrands)
                            )
                ).then(facilityBrands => {
                    $scope.loader=false;
                    console.log("facilityBrands :", facilityBrands);
                    $scope.facilityBrands = facilityBrands;
                });
            }
        );
    }
    $scope.onCustomerSubSubTypeSelected = function() {
        $scope.addNewFacility.selectedFacilityBrand={};
        console.log("selected customer sub type ID:", $scope.addNewFacility.selectedCustomerSubSubType.id);

        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
        ).then(
            accessToken => {
                $scope.loader=true;
        /*        console.log("accessToken:", accessToken);
                console.log("level3: ",$scope.addNewFacility.selectedCustomerSubSubType.id);
                console.log("level2: ",$scope.addNewFacility.selectedCustomerSubType.id);
                console.log("level1: ",$scope.addNewFacility.selectedCustomerType.id);*/
              /*  var customerSegmentId='';
                if($scope.addNewFacility.selectedCustomerSubSubType.id==null || $scope.addNewFacility.selectedCustomerSubSubType.id==undefined){
                    if($scope.addNewFacility.selectedCustomerSubType.id==null || $scope.addNewFacility.selectedCustomerSubType.id==undefined){
                        customerSegmentId=$scope.addNewFacility.selectedCustomerType.id;
                        console.log("aaaaaa",customerSegmentId);


                    }else{
                        customerSegmentId=$scope.addNewFacility.selectedCustomerSubType.id;
                    }
                }
                else {
                    customerSegmentId = $scope.addNewFacility.selectedCustomerSubSubType.id;
                }*/
                $q(
                    resolve =>
                        customerBrandServiceSdk
                            .listLevel1CustomerBrandsWithSegmentId($scope.addNewFacility.selectedCustomerSubSubType.id, accessToken)
                            .then(
                                facilityBrands =>
                                    resolve(facilityBrands)
                            )
                ).then(facilityBrands => {
                    $scope.loader=false;
                    console.log("facilityBrands :", facilityBrands);
                    $scope.facilityBrands = facilityBrands;
                });
            }
        );
    }

    $scope.onFacilityBrandSelected = function() {
        console.log("selected facility brand:", $scope.addNewFacility.selectedFacilityBrand);

        $q(
            resolve =>
                sessionManager
                    .getAccessToken()
                    .then(
                        accessToken =>
                            resolve(accessToken)
                    )
        ).then(
            accessToken => {
                $scope.loader=true;
                console.log("accessToken:", accessToken);
                $q(
                    resolve =>
                        customerBrandServiceSdk
                            .listLevel2CustomerBrandsWithBrandId($scope.addNewFacility.selectedFacilityBrand.id, accessToken)
                            .then(
                                facilitySubBrands =>
                                    resolve(facilitySubBrands)
                            )
                ).then(facilitySubBrands => {
                    $scope.loader=false;
                    console.log("facilitySubBrands :", facilitySubBrands);
                    $scope.facilitySubBrands = facilitySubBrands;
                });
            }
        );
    }

    $scope.addFacility_submit = function(isValid) {



         console.log("onsubmit clicked",$scope.addNewFacility);
         $scope.submitted=true;
         $scope.loader=false;
         if(isValid){
             $scope.loader=true;
             $q(
                 resolve =>
                     sessionManager
                         .getAccessToken()
                         .then(
                             accessToken =>
                                 resolve(accessToken)
                         )
             ).then(
                 accessToken => {
                     $scope.loader=true;
                     console.log("accessToken:", accessToken);
                     var postalAddress = {};
                     var add2=($scope.addNewFacility.addressLine2==undefined)? '':$scope.addNewFacility.addressLine2;
                     postalAddress.street=$scope.addNewFacility.addressLine1.concat(' '+ add2) ;
                     postalAddress.city=$scope.addNewFacility.city;
                     postalAddress.regionIso31662Code=$scope.addNewFacility.selectedRegion.code;
                     postalAddress.postalCode=$scope.addNewFacility.postalCode;
                     postalAddress.countryIso31661Alpha2Code=$scope.addNewFacility.selectedCountry.alpha2Code;
                     console.log("postalAddress",postalAddress);
                     var addCommercialAccountRequest = new AddCommercialAccountReq(
                         $scope.addNewFacility.facilityName,
                         postalAddress,
                         $scope.addNewFacility.phone,
                         $scope.addNewFacility.selectedCustomerType.name,
                        ($scope.addNewFacility.selectedCustomerSubType==undefined)? null:$scope.addNewFacility.selectedCustomerSubType.name,
                         ($scope.addNewFacility.selectedCustomerSubSubType==undefined)? null:$scope.addNewFacility.selectedCustomerSubSubType.name,
                         $scope.addNewFacility.selectedFacilityBrand.name,
                         ($scope.addNewFacility.selectedFacilitySubBrand==undefined)? null:$scope.addNewFacility.selectedFacilitySubBrand.name
                     );
                     console.log("addCommercialAccountRequest",addCommercialAccountRequest);
                     $q(
                         resolve =>
                             accountServiceSdk
                                 .addCommercialAccount(addCommercialAccountRequest, accessToken)
                                 .then(
                                     response =>
                                         resolve(response)
                                 )
                     ).then(response => {
                         $scope.loader=true;
                         console.log("add commercial account response :", response);
                         var addAccountContactReq = new AddAccountContactReq(response,
                             $scope.addNewFacility.contact.firstName,
                             $scope.addNewFacility.contact.lastName,
                             $scope.addNewFacility.contact.phone,
                             $scope.addNewFacility.contact.email,
                             $scope.addNewFacility.selectedCountry.alpha2Code,
                             $scope.addNewFacility.selectedRegion.code);
                         $q(
                             resolve =>
                                 accountContactServiceSdk
                                     .addAccountContact(addAccountContactReq, accessToken)
                                     .then(
                                         response =>
                                             resolve(response)
                                     )
                         ).then(response => {
                             $scope.loader=false;
                             console.log("add account contact response :", response);
                             $scope.modalInstance2 = $uibModal.open({
                                 scope:$scope,
                                 template: '<div class="modal-header"> <h4 class="modal-title">Success !</h4></div>' +
                                 '<div class="modal-body">Facility added successfully</div>' +
                                 '<div class="modal-footer">' +
                                 '<button class="btn btn-primary" type="button" ng-click="ok_addFacility()">ok</button></div>',
                                 size:'sm',
                                 backdrop : 'static'
                             });
                             $scope.ok_addFacility=function(){
                                 $scope.modalInstance2.close();
                                 $scope.modalInstance.close();
                                 $rootScope.finalResponse();
                             };
                         });

                     });
                 }
             );
         }
    };

    $scope.addFacility_cancel=function(){
        $scope.submitted=false;
        $scope.addNewFacility={};
        $scope.modalInstance.dismiss('cancel');
    }
}


angular
    .module(
        "facilityModule",
        []
    )
    .controller("facilityController", facilityController);

facilityController.$inject = ["$scope","$rootScope", "$q","$uibModal","sessionManager", "iso3166Sdk", "customerSegmentServiceSdk", "customerBrandServiceSdk", "accountServiceSdk", "accountContactServiceSdk","$timeout"];
